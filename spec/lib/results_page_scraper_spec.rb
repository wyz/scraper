require_relative "../../lib/results_page_scraper"

describe ResultsPageScraper do
  let(:page) { PaginationScraper.new.get_page_links.first }

  it "should return an array of (up to) 10 events" do
    scraper = described_class.new(page)
    results = scraper.events_list

    expect{ results.count <= 10 }.to be_true
    results.each do |div|
      div.to_s.should match("ListingWhite clearfix")
    end
  end
end