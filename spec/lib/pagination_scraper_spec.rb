require_relative "../../lib/pagination_scraper"

describe PaginationScraper do
  it "should return an array of pagination links" do
    scraper = described_class.new
    results = scraper.get_page_links
    results.each do |link|
      link.should match("http://www.wegottickets.com/searchresults/page/")
    end
  end
end