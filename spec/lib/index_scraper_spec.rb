require_relative "../../lib/index_scraper"

describe IndexScraper do
  it "should return the music search page" do
    scraper = described_class.new
    results = scraper.get_index_page
    results.parser.css("#queryFeedback").text.should match("Genre: Music - General")
  end
end