require_relative "../event_list_builder"

describe EventListBuilder do
  it "should return a list of events containing correct information" do
    builder = described_class.new(page_limit: 2)
    result = builder.to_json
    events_hash = JSON.parse(result)

    events_hash.keys.count.should == 20
    events_hash.values.each do |info|
      info.keys.should =~ ["artist", "price", "venue", "date", "city"]
    end
  end
end