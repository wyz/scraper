require 'mechanize'
require 'pry'
class IndexScraper
  def initialize
    @url = "http://www.wegottickets.com/"
  end

  def get_index_page
    agent          = Mechanize.new
    login_page     = agent.get(@url)
    form           = login_page.form_with(action: '/searchresults/adv')
    form.adv_genre = 11
    agent.submit(form)
  end
end