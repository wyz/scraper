class ArtistNameNormalizer
  attr_accessor :name
  def initialize(text)
    @name = text
  end

  # with this method i wanted to make some logical assumptions
  # on what constitutes the artist name given a string
  # but no more time :)
  def normalize
    name
  end
end