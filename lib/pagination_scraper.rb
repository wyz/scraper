require_relative "index_scraper"

class PaginationScraper
  def initialize(page_limit = nil)
    @index_scraper = IndexScraper.new
    @page_limit = page_limit
  end

  def get_page_links
    index_page = @index_scraper.get_index_page
    links = index_page.parser.css(".pagination_link").map{|link| link[:href]}
    @page_limit ? links.first(@page_limit) : links
  end
end