require_relative "pagination_scraper"

class ResultsPageScraper
  attr_reader :url, :post_params
  def initialize(url, post_params = {})
    @url = url
    @post_params = {adv_genre: 11}.merge post_params
  end

  def events_list
    agent = Mechanize.new
    results_page = agent.post(url, post_params)
    results_page.search("div").css(".ListingWhite.clearfix")
  end
end