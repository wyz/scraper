require 'json'
require_relative "lib/results_page_scraper"
require_relative "lib/normalizers/artist_name_normalizer"

class EventListBuilder
  attr_reader :events, :options
  def initialize(options)
    @options = options

    @pagination_scraper = PaginationScraper.new(options.fetch(:page_limit, nil))
    @page_links = nil
    @events = Hash.new
  end

  def to_json
    (@events.empty? ? build_event_information : @events).to_json
  end

  private
  def build_event_information
    gather_events.each_with_index do |event_element, index|
      artist = normalize_artist_name(event_element.css("a.event_link").text)
      city   = event_element.css(".venuetown").text.sub(":", "")
      venue  = event_element.css(".venuename").text
      date   = event_element.css('p').children.detect{|c| c.is_a? Nokogiri::XML::Text}.text
      price  = event_element.css(".searchResultsPrice").css("strong").text

      @events[index] = {
        artist: artist,
        city: city,
        venue: venue,
        date: date,
        price: price
      }
    end
    @events
  end

  def gather_events
    page_links.map{ |ln| single_page_results ln }.flatten
  end

  def single_page_results(link)
    page_scraper = ResultsPageScraper.new link
    page_scraper.events_list
  end

  def page_links
    @page_links ||= @pagination_scraper.get_page_links
  end

  def normalize_artist_name(name)
    ArtistNameNormalizer.new(name).normalize
  end
end