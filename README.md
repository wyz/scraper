### How to use
* you can run `rspec` or
* open an irb console
  * `require <# path to event_list_builder.rb>` (wou can find it if you `pwd` inside project directory)
  * then execute `EventListBuilder.new(page_limit: 2).to_json`
    * note - **page_limit** *is there for the sake of simplicity*

###Logic
**EventListBuilder** scrapes all pages given back by **ResultsPageScraper** with links supplied by **PaginationScraper**. The results are in form of **div blocks** each one representin one element on the web page (from those 10 that the website provides in sarch results)
**PaginationScraper** takes it's links with the help of **IndexScraper**. The latest object mentioned is submitting a form on the original website and returning the resulting page.

* **note** - PaginationScraper only takes the links it sees, but there can easily be modified to take the oldest one (eg. page 306) and incrementally building the pages until that number. **I thought it's not a problem for this exercise.**

###Third-parties
The solution is achieved with the help of **Mechanize** and **Nokogiri**

###Testing
The project is **integration tested**

I wanted to introduce **VCR** in the tests so that the webpages are not hit for every test run, but there was no more time

###Final thoughts
Good luck and be [irresponsible](https://twitter.com/ariejan/status/350936170923950082/photo/1) :)